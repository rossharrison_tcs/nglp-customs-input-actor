FROM java:openjdk-8-jre

ADD nglp-customs-input-actor-all.jar nglp-customs-input-actor-all.jar

CMD java -jar nglp-customs-input-actor-all.jar
