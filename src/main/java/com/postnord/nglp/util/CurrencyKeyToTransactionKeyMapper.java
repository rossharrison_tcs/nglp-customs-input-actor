package com.postnord.nglp.util;

import com.postnord.nglp.processor.model.CurrencyCheckModel;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

public class CurrencyKeyToTransactionKeyMapper implements KeyValueMapper<String, CurrencyCheckModel, KeyValue<String, CurrencyCheckModel>> {

    @Override
    public KeyValue<String, CurrencyCheckModel> apply(String key, CurrencyCheckModel value) {
        String transactionId = value.getTransaction().getTransactionId().getTransactionId();
        return new KeyValue<>(transactionId, value);
    }
}
