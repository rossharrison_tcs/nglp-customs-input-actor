package com.postnord.nglp.util;

import org.apache.kafka.streams.kstream.ValueMapper;

import com.postnord.nglp.constant.CustomConstant;
import com.postnord.nglp.model.EventType;
import com.postnord.potapi.model.Item;
import com.postnord.potapi.model.Order;
import com.postnord.potapi.model.Payload;
import com.postnord.potapi.model.Transaction;
import com.postnord.potapi.model.Transaction.Builder;
import com.postnord.potapi.model.types.Customs;
import com.postnord.potapi.model.types.CustomsOriginal;
import com.postnord.potapi.model.types.PartyInfo;
import com.postnord.potapi.model.types.PartyOriginal;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;

public class EventToTransactionMapper implements ValueMapper<EventType, Transaction> {

	@Override
	public Transaction apply(EventType event) {
		Transaction.Builder builder = new Builder();
		UUID transactionId = UUID.randomUUID();
		ZonedDateTime datetime = getDateTime(event.getDate());
		return builder.withId(transactionId.toString())
                    .withEventTime(datetime== null ? null : datetime.toString())
                    .withRetries(0)
                    .withAction(CustomConstant.WRITE)
                    .withActor(CustomConstant.GLP)
                    .withPayload(buildPayload(event))
                 .build();
	}
	private static ZonedDateTime getDateTime(XMLGregorianCalendar timestamp){
		if(timestamp == null){
			return null;
		}
		final Instant instant = timestamp.toGregorianCalendar().toInstant();
		return  ZonedDateTime.ofInstant(instant, ZoneId.of("UTC"));
	}

	private Payload buildPayload(EventType event) {
		Payload.Builder builder = new com.postnord.potapi.model.Payload.Builder();
		return builder.withItem(buildItem(event)).withOrder(buildOrder(event)).build();

	}

	private Order buildOrder(EventType event) {
		Order.Builder builer = new com.postnord.potapi.model.Order.Builder();
		return builer.withConsignorPartyInfo(buildConsignorPartyInfo(event))
				.withConsigneePartyInfo(buildConsigneePartyInfoe(event)).build();
	}

	private PartyInfo buildConsigneePartyInfoe(EventType event) {
		PartyInfo.Builder builder = new com.postnord.potapi.model.types.PartyInfo.Builder();
		return builder.withOriginal(buildConsigneePartyOriginal(event)).build();
	}

	private PartyOriginal buildConsigneePartyOriginal(EventType event) {
		PartyOriginal.Builder builder = new com.postnord.potapi.model.types.PartyOriginal.Builder();
		return builder.withName(event.getConsignee() == null ? null : event.getConsignee().getName())
				.withPhone(event.getConsignee() == null ? null
						: event.getConsignee().getContact() == null ? null
								: event.getConsignee().getContact().getPhone())

				.withAddressee(event.getConsignee() == null ? null
						: event.getConsignee().getAddress() == null ? null
								: event.getConsignee().getAddress().getStreet1())
				.withAddressRow(event.getConsignee() == null ? null
						: event.getConsignee().getAddress() == null ? null
								: event.getConsignee().getAddress().getStreet2())
				.withCity(event.getConsignee() == null ? null
						: event.getConsignee().getAddress() == null ? null
								: event.getConsignee().getAddress().getCity())
				.withCountry(
						event.getConsignee() == null ? null
								: event.getConsignee().getAddress() == null ? null
										: event.getConsignee().getAddress().getCountry())
				.withPostalCode(event.getConsignee() == null ? null
						: event.getConsignee().getAddress() == null ? null
								: event.getConsignee().getAddress().getPostcode())
				.build();
	}

	private PartyInfo buildConsignorPartyInfo(EventType event) {
		PartyInfo.Builder builder = new com.postnord.potapi.model.types.PartyInfo.Builder();
		return builder.withOriginal(buildConsignorPartyOriginal(event)).build();
	}

	private PartyOriginal buildConsignorPartyOriginal(EventType event) {
		PartyOriginal.Builder builder = new com.postnord.potapi.model.types.PartyOriginal.Builder();
		return builder
				.withAddressee(event.getConsignor() == null ? null
						: event.getConsignor().getAddress() == null ? null
								: event.getConsignor().getAddress().getStreet1())
				.withAddressRow(event.getConsignor() == null ? null
						: event.getConsignor().getAddress() == null ? null
								: event.getConsignor().getAddress().getStreet2())
				.withCity(event.getConsignor() == null ? null
						: event.getConsignor().getAddress() == null ? null
								: event.getConsignor().getAddress().getCity())
				.withCountry(
						event.getConsignor() == null ? null
								: event.getConsignor().getAddress() == null ? null
										: event.getConsignor().getAddress().getCountry())
				.withPostalCode(event.getConsignor() == null ? null
						: event.getConsignor().getAddress() == null ? null
								: event.getConsignor().getAddress().getPostcode())
				.build();
	}

	private Item buildItem(EventType event) {

		Item.Builder builder = new com.postnord.potapi.model.Item.Builder();

		if(event.getCustomsLabel() != null && event.getCustomsLabel().getTotalStatedWeight() != null
				&& event.getCustomsLabel().getTotalStatedWeight().getUnit().equalsIgnoreCase("Kg")){
			event.getCustomsLabel().getTotalStatedWeight().setValue(
					event.getCustomsLabel().getTotalStatedWeight().getValue().multiply(new BigDecimal("1000")));
			event.getCustomsLabel().getTotalStatedWeight().setUnit("g");
		}

        ZonedDateTime datetime = getDateTime(event.getDate());

		return builder.withItemId(UUID.nameUUIDFromBytes(event.getId().getBytes()).toString())
                .withPnItemId(event.getId())
				.withProductCode(event.getService() != null ? event.getService().getCode() : null)
				.withProcessingTime(datetime == null ? null : datetime.toString())
				.withServiceCode(event.getService() == null ? null : event.getAdditionalService().getCode())
				.withWeight(event.getCustomsLabel() == null ? null :
						event.getCustomsLabel().getTotalStatedWeight() == null ? null :
								event.getCustomsLabel().getTotalStatedWeight().getValue().intValue())
				.withCustoms(buildCustoms(event))
				.build();
	}

	private Customs buildCustoms(EventType event) {

		Customs.Builder builder = new com.postnord.potapi.model.types.Customs.Builder();

		return builder
				.withOriginal(buildCustomOriginal(event))
				.build();
	}

	private CustomsOriginal buildCustomOriginal(EventType event) {
		CustomsOriginal.Builder builder = new com.postnord.potapi.model.types.CustomsOriginal.Builder();

		try{

			builder.withGoodsDescription(
							event.getCustomsLabel() == null ? null : event.getCustomsLabel().getItemDescription())
					.withIsGift(event.getCustomsLabel() == null ? null : event.getCustomsLabel().isIsGift())
					.withIsDocument(event.getCustomsLabel() == null ? null : event.getCustomsLabel().isIsDocument())
					.withIsSample(event.getCustomsLabel() == null ? null : event.getCustomsLabel().isIsCommercialSample())
					.withIsCommercialRecipient(
							event.getCustomsLabel() == null ? null : event.getCustomsLabel().isIsCommercialRecipient())
					.withGoodsValue(
							event.getCustomsLabel() == null ? null
									: event.getCustomsLabel().getTotalStatedValue() == null ? null
									: event.getCustomsLabel().getTotalStatedValue().getValue() == null ? null
									: event.getCustomsLabel().getTotalStatedValue().getValue()
									.doubleValue(),
							event.getCustomsLabel() == null ? null
									: event.getCustomsLabel().getTotalStatedValue() == null ? null
									: event.getCustomsLabel().getTotalStatedValue().getCurrency())
			.withHsTariffNumber(event.getLocalCode() != null ? event.getLocalCode().getValue() : null);

		}catch(IllegalArgumentException e){
			System.out.println("Currency received : " + event.getCustomsLabel().getTotalStatedValue().getCurrency());
		}
		return builder.build();
	}

	public String getInputDateProcessed(String date) {

		System.out.println(date);

		if (date == null) {
			return null;
		} else if (date.contains(CustomConstant.T) && date.contains(CustomConstant.PLUS)
				&& date.contains(CustomConstant.Z)) {
			int startIndex = date.indexOf(CustomConstant.PLUS);
			date = date.substring(0, startIndex) + CustomConstant.Z;
		} else if (date.contains(CustomConstant.T) && date.contains(CustomConstant.PLUS)) {
			int startIndex = date.indexOf(CustomConstant.PLUS);
			date = date.substring(0, startIndex) + CustomConstant.Z;
		} else if (date.contains(CustomConstant.T)) {
			date = date + CustomConstant.Z;
		}

		return date;

	}

}
