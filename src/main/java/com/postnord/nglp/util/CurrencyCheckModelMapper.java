package com.postnord.nglp.util;

import com.postnord.nglp.processor.model.CurrencyCheckModel;
import com.postnord.potapi.model.Transaction;
import org.apache.kafka.streams.kstream.ValueMapper;

public class CurrencyCheckModelMapper implements ValueMapper<Transaction, CurrencyCheckModel> {

    @Override
    public CurrencyCheckModel apply(Transaction value) {

        String currencyCode = getCurrencyCode(value);
        return new CurrencyCheckModel(false, false, value, currencyCode);
    }

    private String getCurrencyCode(Transaction value){
        String currencyCode = null;
        if(value.getPayload() != null &&
        value.getPayload().getItems() != null &&
        !value.getPayload().getItems().isEmpty() &&
        value.getPayload().getItems().get(0).getCustoms() != null &&
        value.getPayload().getItems().get(0).getCustoms().getOriginals() != null &&
        value.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getGoodsValue() != null &&
        value.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getGoodsValue().getCurrency() != null){
            currencyCode = value.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getGoodsValue().getCurrency().getCurrencyCode();
        }
        return  currencyCode;
    }
}
