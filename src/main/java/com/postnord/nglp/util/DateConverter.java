package com.postnord.nglp.util;

import com.postnord.nglp.constant.CustomConstant;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateConverter {

    public XMLGregorianCalendar getXMLGregorianCalendar(String dateTime) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
        GregorianCalendar cal = new GregorianCalendar();
        XMLGregorianCalendar xmlGregCal = null;
        try {
            Date date = format.parse(getInputDateProcessed(dateTime));
            cal.setTime(date);
            xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return xmlGregCal;
    }


    public String getStringFromXMLGregorianCalendar(XMLGregorianCalendar cal)
    {
        Calendar calendar = cal.toGregorianCalendar();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss.SSS'Z'");
        return formatter.format(calendar.getTime());
    }

    public String getInputDateProcessed(String date) {
        if (date == null) {
            return null;
        } else if (date.contains(CustomConstant.T) && date.contains(CustomConstant.PLUS)
                && date.contains(CustomConstant.Z)) {
            int startIndex = date.indexOf(CustomConstant.PLUS);
            date = date.substring(0, startIndex) + CustomConstant.Z;
        } else if (date.contains(CustomConstant.T) && date.contains(CustomConstant.PLUS)) {
            int startIndex = date.indexOf(CustomConstant.PLUS);
            date = date.substring(0, startIndex) + CustomConstant.Z;
        } else if (date.contains(CustomConstant.T)) {
            date = date + CustomConstant.Z;
        }

        return date;

    }

}
