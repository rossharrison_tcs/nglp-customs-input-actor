package com.postnord.nglp.util;

import com.postnord.nglp.processor.model.CurrencyCheckModel;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

public class CurrencyKeyMapper implements KeyValueMapper<String, CurrencyCheckModel, KeyValue<String, CurrencyCheckModel>> {

    @Override
    public KeyValue<String, CurrencyCheckModel> apply(String key, CurrencyCheckModel value) {
        return new KeyValue<>(value.getCurrencyCode(), value);
    }
}
