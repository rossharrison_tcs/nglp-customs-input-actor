package com.postnord.nglp.util;

import com.postnord.nglp.processor.model.CurrencyCheckModel;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.postnord.potapi.model.Transaction;

public class TransactionToJsonStringMapper implements KeyValueMapper<String, CurrencyCheckModel, KeyValue<String, String>> {

	private static final Logger LOG = LoggerFactory.getLogger(TransactionToJsonStringMapper.class);

	@Override
	public KeyValue<String, String> apply(String k, CurrencyCheckModel value) {
		String key = value.getTransaction().getTransactionId().getTransactionId();
		String transactionAsJsonString = value.getTransaction().toJson();
		LOG.debug("key :  " + key);
		LOG.debug("Output JSON Message :  " + transactionAsJsonString);
		return new KeyValue<String, String>(key, transactionAsJsonString);
	}
}
