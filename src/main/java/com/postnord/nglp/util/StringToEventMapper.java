package com.postnord.nglp.util;

import java.io.StringReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.kafka.streams.kstream.ValueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.postnord.nglp.constant.CustomConstant;
import com.postnord.nglp.model.AccountType;
import com.postnord.nglp.model.AdditionalServiceType;
import com.postnord.nglp.model.AddressType;
import com.postnord.nglp.model.AmountType;
import com.postnord.nglp.model.CodedAddressType;
import com.postnord.nglp.model.ConsigneeType;
import com.postnord.nglp.model.ConsignorType;
import com.postnord.nglp.model.ContactType;
import com.postnord.nglp.model.CustomsLabelType;
import com.postnord.nglp.model.EventType;
import com.postnord.nglp.model.LocalCodeType;
import com.postnord.nglp.model.LocationType;
import com.postnord.nglp.model.MeasurementType;
import com.postnord.nglp.model.ResultsStatusType;
import com.postnord.nglp.model.ServiceType;
import com.postnord.nglp.model.WeightType;
import com.postnord.nglp.util.DateConverter;

public class StringToEventMapper implements ValueMapper<String, EventType> {

    private static final Logger LOG = LoggerFactory.getLogger(StringToEventMapper.class);

    @Override
    public EventType apply(String message) {

        LOG.debug("Start :: Converting input byte [] to Event POJO");
        EventType event;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DateConverter conversion = new DateConverter();
        event = new EventType();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(message)));
            NodeList eventNode = document.getElementsByTagName(CustomConstant.TASKRESULTS);


            for (int i = 0; i < eventNode.getLength(); i++) {
                Node parentNode = eventNode.item(i);
                if (parentNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element parentElement = (Element) parentNode;
                    event.setId(parentElement.getAttribute(CustomConstant.ID));
                    //event.setDate(OffsetDateTime.parse(parentElement.getAttribute(CustomConstant.DATE), DateTimeFormatter.ISO_ZONED_DATE_TIME));
                    event.setDate(getXMLGregorianCalendar(parentElement.getAttribute(CustomConstant.DATE)));
                    event.setUserId(parentElement.getAttribute(CustomConstant.USER_ID));
                    event.setSchemaVersion(new BigDecimal(parentElement.getAttribute(CustomConstant.SCHEMA_VERSION)));
                    NodeList childNode = parentElement.getChildNodes();
                    for (int j = 0; j < childNode.getLength(); j++) {
                        Node child = childNode.item(j);
                        if (child.getNodeType() == Node.ELEMENT_NODE) {
                            Element childElement = (Element) child;
                            if (childElement.getTagName().equals(CustomConstant.LOCAL_CODE)) {
                                LocalCodeType localCodeType = new LocalCodeType();
                                localCodeType.setType(childElement.getAttribute(CustomConstant.TYPE));
                                localCodeType.setValue(childElement.getTextContent());
                                event.setLocalCode(localCodeType);
                            } else if (childElement.getTagName().equals(CustomConstant.LOCATION)) {
                                LocationType locationType = new LocationType();
                                locationType.setId(childElement.getAttribute(CustomConstant.ID));
                                locationType.setName(childElement.getAttribute(CustomConstant.TYPE));
                                locationType.setType(childElement.getAttribute(CustomConstant.NAME));
                                event.setLocation(locationType);
                            } else if (childElement.getTagName().equals(CustomConstant.SERVICE)) {
                                ServiceType serviceType = new ServiceType();
                                serviceType.setCode(childElement.getAttribute(CustomConstant.CODE));
                                event.setService(serviceType);
                            } else if (childElement.getTagName().equals(CustomConstant.MEASUREMENT)) {
                                MeasurementType measurementType = new MeasurementType();
                                WeightType weightType = new WeightType();
                                NodeList measurement = childElement.getChildNodes();
                                for (int k = 0; k < measurement.getLength(); k++) {
                                    Node cchild = measurement.item(k);
                                    if (cchild.getNodeType() == Node.ELEMENT_NODE) {
                                        Element cchildElement = (Element) cchild;
                                        if (cchildElement.getTagName() == CustomConstant.WEIGHT) {
                                            weightType.setUnit(cchildElement.getAttribute(CustomConstant.UNIT));
                                            weightType.setValue(new BigDecimal(cchildElement.getTextContent()));
                                        }
                                    }
                                }

                                measurementType.setWeight(weightType);
                                event.setMeasurement(measurementType);

                            } else if (childElement.getTagName() == CustomConstant.ADDITIONAL_SERVICE) {
                                AdditionalServiceType additionalServiceType = new AdditionalServiceType();
                                AccountType accountType = new AccountType();
                                AmountType amountType = new AmountType();
                                NodeList additionalService = childElement.getChildNodes();
                                for (int k = 0; k < additionalService.getLength(); k++) {
                                    Node cchild = additionalService.item(k);
                                    if (cchild.getNodeType() == Node.ELEMENT_NODE) {
                                        Element cchildElement = (Element) cchild;
                                        if (cchildElement.getTagName() == CustomConstant.ACCOUNT) {
                                            accountType.setType(cchildElement.getAttribute(CustomConstant.TYPE));
                                            accountType.setValue(cchildElement.getTextContent());
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.AMOUNT) {
                                            amountType.setCurrency(cchildElement.getAttribute(CustomConstant.CURRENCY));
                                            amountType.setValue(new BigDecimal(cchildElement.getTextContent()));
                                        }
                                    }
                                }
                                additionalServiceType.setCode(childElement.getAttribute(CustomConstant.CODE));
                                additionalServiceType.setAccount(accountType);
                                additionalServiceType.setAmount(amountType);
                                event.setAdditionalService(additionalServiceType);
                            } else if (childElement.getTagName() == CustomConstant.CONSIGNOR) {
                                NodeList consignors = childElement.getChildNodes();
                                ConsignorType consignorType = new ConsignorType();
                                AddressType addressType = new AddressType();
                                for (int k = 0; k < consignors.getLength(); k++) {
                                    Node cchild = consignors.item(k);
                                    if (cchild.getNodeType() == Node.ELEMENT_NODE) {
                                        Element cchildElement = (Element) cchild;
                                        NodeList address = cchildElement.getChildNodes();
                                        for (int l = 0; l < address.getLength(); l++) {
                                            Node addchild = address.item(l);
                                            if (addchild.getNodeType() == Node.ELEMENT_NODE) {
                                                Element addChildElement = (Element) addchild;
                                                if (addChildElement.getTagName() == CustomConstant.STREET1) {
                                                    addressType.setStreet1(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.STREET2) {
                                                    addressType.setStreet2(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.POSTCODE) {
                                                    addressType.setPostcode(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.CITY) {
                                                    addressType.setCity(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.COUNTRY) {
                                                    addressType.setCountry(addChildElement.getTextContent());
                                                }
                                            }
                                        }
                                    }
                                }
                                consignorType.setAddress(addressType);
                                event.setConsignor(consignorType);
                            } else if (childElement.getTagName() == CustomConstant.CONSIGNEE) {
                                NodeList consignees = childElement.getChildNodes();
                                ConsigneeType consigneeType = new ConsigneeType();
                                AddressType addressType = new AddressType();
                                ContactType contactType = new ContactType();

                                for (int k = 0; k < consignees.getLength(); k++) {
                                    Node cchild = consignees.item(k);
                                    if (cchild.getNodeType() == Node.ELEMENT_NODE) {
                                        Element cchildElement = (Element) cchild;
                                        if (cchildElement.getTagName() == CustomConstant.NAME) {
                                            consigneeType.setName(cchildElement.getTextContent());
                                        }
                                        NodeList address = cchildElement.getChildNodes();

                                        for (int l = 0; l < address.getLength(); l++) {
                                            Node addchild = address.item(l);
                                            if (addchild.getNodeType() == Node.ELEMENT_NODE) {
                                                Element addChildElement = (Element) addchild;

                                                if (addChildElement.getTagName() == CustomConstant.STREET1) {
                                                    addressType.setStreet1(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.STREET2) {
                                                    addressType.setStreet2(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.POSTCODE) {
                                                    addressType.setPostcode(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.CITY) {
                                                    addressType.setCity(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.COUNTRY) {
                                                    addressType.setCountry(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.MOBILE) {
                                                    contactType.setMobile(addChildElement.getTextContent());
                                                }
                                                if (addChildElement.getTagName() == CustomConstant.PHONE) {
                                                    contactType.setPhone(addChildElement.getTextContent());
                                                }
                                            }
                                        }
                                    }
                                }
                                consigneeType.setAddress(addressType);
                                consigneeType.setContact(contactType);
                                event.setConsignee(consigneeType);
                            } else if (childElement.getTagName() == CustomConstant.CODED_ADDRESS) {
                                NodeList codedAddress = childElement.getChildNodes();
                                CodedAddressType codedAddressType = new CodedAddressType();
                                for (int k = 0; k < codedAddress.getLength(); k++) {
                                    Node cchild = codedAddress.item(k);
                                    if (cchild.getNodeType() == Node.ELEMENT_NODE) {
                                        Element cchildElement = (Element) cchild;

                                        if (cchildElement.getTagName() == CustomConstant.POSTCODE) {
                                            codedAddressType.setPostcode(cchildElement.getTextContent());
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.STOPPOINT) {
                                            codedAddressType.setStopPoint(cchildElement.getTextContent());
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.DELIVERYPOINT) {
                                            codedAddressType.setDeliveryPoint(cchildElement.getTextContent());
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.HOUSINGID) {
                                            codedAddressType.setHousingId(cchildElement.getTextContent());
                                        }
                                    }

                                }
                                event.setCodedAddress(codedAddressType);
                            } else if (childElement.getTagName() == CustomConstant.CUSTOMS_LABEL) {
                                NodeList customsLabels = childElement.getChildNodes();
                                CustomsLabelType customsLabelType = new CustomsLabelType();
                                WeightType weightType = new WeightType();
                                AmountType amountType = new AmountType();
                                for (int k = 0; k < customsLabels.getLength(); k++) {
                                    Node cchild = customsLabels.item(k);
                                    if (cchild.getNodeType() == Node.ELEMENT_NODE) {
                                        Element cchildElement = (Element) cchild;
                                        if (cchildElement.getTagName() == CustomConstant.ITEM_DESCRIPTION) {
                                            customsLabelType.setItemDescription(cchildElement.getTextContent());
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.IS_GIFT) {
                                            customsLabelType.setIsGift(new Boolean(cchildElement.getTextContent()));
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.IS_DOCUMENT) {
                                            customsLabelType.setIsDocument(new Boolean(cchildElement.getTextContent()));
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.IS_COMMERCIAL_SAMPLE) {
                                            customsLabelType
                                                    .setIsCommercialSample(new Boolean(cchildElement.getTextContent()));
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.IS_OTHER) {
                                            customsLabelType.setIsOther(new Boolean(cchildElement.getTextContent()));
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.IS_COMMERCIAL_RECIPIENT) {
                                            customsLabelType.setIsCommercialRecipient(
                                                    new Boolean(cchildElement.getTextContent()));
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.ORIGINATING_COUNTRY) {
                                            customsLabelType.setOriginatingCountry(cchildElement.getTextContent());
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.TOTAL_STATED_WEIGHT) {
                                            weightType.setUnit(cchildElement.getAttribute(CustomConstant.UNIT));
                                            weightType.setValue(new BigDecimal(cchildElement.getTextContent()));
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.TOTAL_STATED_VALUE) {
                                            amountType.setCurrency(cchildElement.getAttribute(CustomConstant.CURRENCY));
                                            amountType.setValue(new BigDecimal(cchildElement.getTextContent()));
                                        }
                                    }

                                }
                                customsLabelType.setTotalStatedValue(amountType);
                                customsLabelType.setTotalStatedWeight(weightType);
                                event.setCustomsLabel(customsLabelType);
                            } else if (childElement.getTagName() == CustomConstant.RESULTSSTATUS) {
                                NodeList resultsStatus = childElement.getChildNodes();
                                ResultsStatusType resultsStatusType = new ResultsStatusType();
                                for (int k = 0; k < resultsStatus.getLength(); k++) {
                                    Node cchild = resultsStatus.item(k);
                                    if (cchild.getNodeType() == Node.ELEMENT_NODE) {
                                        Element cchildElement = (Element) cchild;
                                        if (cchildElement.getTagName() == CustomConstant.TASKREJECTED) {
                                            resultsStatusType.setTaskRejected(new Boolean(cchildElement.getTextContent()));
                                        }
                                        if (cchildElement.getTagName() == CustomConstant.TASKCOMPLETED) {
                                            resultsStatusType.setTaskCompleted(new Boolean(cchildElement.getTextContent()));
                                        }
                                    }
                                }
                                event.setResultsStatus(resultsStatusType);
                            }

                        }
                    }
                }
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        LOG.debug("End :: Converting input byte [] to Event POJO");
        LOG.debug(event.toString());
        return event;

    }

    public XMLGregorianCalendar getXMLGregorianCalendar(String dateTime) {
        XMLGregorianCalendar xmlGregCal = null;
        try {
            ZonedDateTime date = ZonedDateTime.parse(dateTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            xmlGregCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(GregorianCalendar.from(date));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return xmlGregCal;
    }
}
