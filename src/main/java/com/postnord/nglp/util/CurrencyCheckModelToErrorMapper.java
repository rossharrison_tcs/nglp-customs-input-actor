package com.postnord.nglp.util;

import com.postnord.nglp.processor.model.CurrencyCheckModel;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

public class CurrencyCheckModelToErrorMapper implements KeyValueMapper<String, CurrencyCheckModel, KeyValue<String, String>> {
    @Override
    public KeyValue<String, String> apply(String key, CurrencyCheckModel value) {
        return new KeyValue<>(value.getCurrencyCode(), value.getTransaction().toJson());
    }
}
