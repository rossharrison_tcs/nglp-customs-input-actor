package com.postnord.nglp.processor.model;

import com.postnord.potapi.model.Transaction;

public class CurrencyCheckModel {

    private boolean isPresentInCurrencyTopic;
    private boolean isPresentInNglpCUrrencyTopic;
    private Transaction transaction;
    private String currencyCode;

    public CurrencyCheckModel(boolean isPresentInCurrencyTopic, boolean isPresentInNglpCUrrencyTopic,
                              Transaction transaction, String currencyCode){
        super();
        this.isPresentInCurrencyTopic = isPresentInCurrencyTopic;
        this.isPresentInNglpCUrrencyTopic = isPresentInNglpCUrrencyTopic;
        this.transaction = transaction;
        this.currencyCode = currencyCode;

    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public boolean isPresentInCurrencyTopic() {
        return isPresentInCurrencyTopic;
    }

    public void setPresentInCurrencyTopic(boolean presentInCurrencyTopic) {
        isPresentInCurrencyTopic = presentInCurrencyTopic;
    }

    public boolean isPresentInNglpCUrrencyTopic() {
        return isPresentInNglpCUrrencyTopic;
    }

    public void setPresentInNglpCUrrencyTopic(boolean presentInNglpCUrrencyTopic) {
        isPresentInNglpCUrrencyTopic = presentInNglpCUrrencyTopic;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
