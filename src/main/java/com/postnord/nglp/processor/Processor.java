package com.postnord.nglp.processor;

import java.util.Properties;

import com.postnord.nglp.processor.model.CurrencyCheckModel;
import com.postnord.nglp.util.*;
import com.postnord.potapi.model.types.Money;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.postnord.nglp.constant.CustomConstant;
import com.postnord.nglp.model.EventType;
import com.postnord.potapi.model.Transaction;

public class Processor {

	private static final Logger LOG = LoggerFactory.getLogger(Processor.class);

	private KafkaStreams kafkaStreams;

	public void process() {
		LOG.debug("Start of process()");
		kafkaStreams = new KafkaStreams(topology(), config());
		kafkaStreams.start();
		LOG.debug("Start of process()");
	}

	private Properties config() {
		LOG.debug("Start of config()");
		return KafkaUtil.getStreamsApplicationProperties(CustomConstant.APPLICATION_ID);
	}

	public Topology topology() {

		LOG.debug("Start of topology()");
		StreamsBuilder builder = new StreamsBuilder();

		final GlobalKTable<String, String> currencyFilter = getCurrencyFilterTable(builder);

		
		final GlobalKTable<String, String> nglpCurrencyFilter = getNglpCurrencyFilterTable(builder);

		KStream<String, Transaction> eventStream = builder
				.stream(inputTopic(), consumeEventResult())
				.filter(isNull())
				.mapValues(stringToEventMapper())
				.filter(isTransactionCompleted())
				.mapValues(eventToPotapi());

		KStream<String, CurrencyCheckModel> currencyCheckApplied = eventStream
				.mapValues(transactionToCurrencyCheckModel())
				.map(CurrencyKeyMapper())
				.leftJoin(currencyFilter, (currencyCode, currencyCheckModel) -> currencyCode,
						(currencyCheckModel, currencyValue) ->{
							if(currencyValue != null){
								currencyCheckModel.setPresentInCurrencyTopic(true);
							}
							return currencyCheckModel;
						})
				.map(currencyKeyToTransactionKeyMapper());

		KStream<String, CurrencyCheckModel> glpCurrencycheckApplied = currencyCheckApplied
				.map(CurrencyKeyMapper())
				.leftJoin(nglpCurrencyFilter, (currencyCode, currencyCheckModel) -> currencyCode,
						(currencyCheckModel, currencyValue) ->{
							if(currencyValue != null){
								currencyCheckModel.setPresentInNglpCUrrencyTopic(true);
							}
							return currencyCheckModel;
						})
				.map(currencyKeyToTransactionKeyMapper());

		final KStream<String, CurrencyCheckModel>[] streamBranches =
				glpCurrencycheckApplied.branch( (key, currencyCheckModel) ->
						!(currencyCheckModel.isPresentInCurrencyTopic() || currencyCheckModel.isPresentInNglpCUrrencyTopic()));

		streamBranches[0]
				.map(currencyCheckModelToErrorMapper()).to(errorTopic(), produceError());

		glpCurrencycheckApplied
				.map(transactionToJsonString())
				.to(outputTopic(), produceTransactionString());

		return builder
				.build();
	}

	private KeyValueMapper<String, CurrencyCheckModel, KeyValue<String, String>> currencyCheckModelToErrorMapper() {
		return new CurrencyCheckModelToErrorMapper();
	}

	private KeyValueMapper<String, CurrencyCheckModel, KeyValue<String, CurrencyCheckModel>> currencyKeyToTransactionKeyMapper() {
		return new CurrencyKeyToTransactionKeyMapper();
	}

	private ValueMapper<Transaction, CurrencyCheckModel> transactionToCurrencyCheckModel() {
		return new CurrencyCheckModelMapper();
	}

	private KeyValueMapper<String, CurrencyCheckModel, KeyValue<String, CurrencyCheckModel>> CurrencyKeyMapper() {
		return new CurrencyKeyMapper();
	}

	private KeyValueMapper<String, CurrencyCheckModel, KeyValue<String, String>> transactionToJsonString() {
		return new TransactionToJsonStringMapper();
	}

	private ValueMapper<String, EventType> stringToEventMapper() {
		return new StringToEventMapper();
	}

    private Predicate<String, EventType> isTransactionCompleted() {
        return (key, event) -> event.getResultsStatus().isTaskCompleted();
    }

	private ValueMapper<EventType, Transaction> eventToPotapi() {
		return new EventToTransactionMapper();
	}

	private Consumed<String, String> consumeEventResult() {

		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();

		return Consumed.with(keySerde, valueSerde);
	}

	private GlobalKTable<String, String> getCurrencyFilterTable(StreamsBuilder streamsBuilder) {
		return streamsBuilder.globalTable(currencyFilterTopic(), Consumed.with(Serdes.String(), Serdes.String()));
	}

	private GlobalKTable<String, String> getNglpCurrencyFilterTable(StreamsBuilder streamsBuilder) {
		return streamsBuilder.globalTable(nglpCurrencyFilterTopic(), Consumed.with(Serdes.String(), Serdes.String()));
	}

	private String currencyFilterTopic() {
		return SystemUtil.getEnvironmentVariable("CURRENCY_FILTER_TOPIC", "prod-vat2-currency-filter");
	}

	private String errorTopic() {
		return SystemUtil.getEnvironmentVariable("ERROR_TOPIC", "prod-vat2-error");
	}

	private String nglpCurrencyFilterTopic() {
		return SystemUtil.getEnvironmentVariable("NGLP_CURRENCY_FILTER_TOPIC", "prod-vat2-nglp-currency-filter");
	}

	private String inputTopic() {
		return SystemUtil.getEnvironmentVariable("INPUT_TOPIC", CustomConstant.PROD_GLP_SE_CUSTOMS_EVENT );
	}

	private Predicate<String, String> isNull() {
		return (k, v) -> v != null;
	}

	private Produced<String, String> produceTransactionString() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();
		return Produced.with(keySerde, valueSerde);
	}

	private Produced<String, String> produceError() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();
		return Produced.with(keySerde, valueSerde);
	}

	private String outputTopic() {
		return SystemUtil.getEnvironmentVariable("OUTPUT_TOPIC",CustomConstant.POTAPI_TRANSACTION_READ);
	}

}
