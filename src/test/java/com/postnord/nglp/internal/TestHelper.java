package com.postnord.nglp.internal;

import com.postnord.nglp.model.EventType;
import com.postnord.potapi.model.util.JsonUtil;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class TestHelper {

    public static InputStream getFileAsStream(String resource) throws IOException {
        File initialFile = new File("src/test/resources/" + resource);
        return new FileInputStream(initialFile);
    }

    private static String getStreamSample(String fileName) throws IOException {

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getFileAsStream(fileName), StandardCharsets.UTF_8))) {
            return bufferedReader.lines().collect(Collectors.joining());
        }
    }

    public static EventType getEvent(String fileName) throws IOException{

        JsonUtil<EventType> utils = new JsonUtil<>();
        EventType event = utils.fromJson(getStreamSample(fileName), EventType.class);
        return event;
    }

    public static String getXml(String fileName) throws  IOException{
        return getStreamSample(fileName);
    }

    public static List<ConsumerRecord<byte[],byte[]>> getValues(String name, String topic) throws IOException{

        List<ConsumerRecord<byte[],byte[]>> consumerRecords = new ArrayList<>();
        String resourceName = "src/test/resources/internal/" + name;
        Scanner scanner = new Scanner(new FileReader(resourceName));
        String input ;
        int counter = 0;

        while(scanner.hasNext()){
            input = scanner.nextLine();
            String keyValues[] = input.split(":");
            ConsumerRecord<byte[], byte[]> consumer = new ConsumerRecord<>(topic,0,
                    counter,keyValues[0].getBytes(),keyValues[1].getBytes());
            counter++;
            consumerRecords.add(consumer);
        }
        return consumerRecords;
    }
}
