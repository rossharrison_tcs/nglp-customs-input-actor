package com.postnord.nglp.internal;

public class Constants {

    public static final String PROD_GLP_SE_CUSTOMS_EVENT = "prod-glp-se-customs-event";
    public static final String POTAPI_TRANSACTION_READ = "potapi-transaction-read";
    public static final String CURENCY_LIST = "currency-list.properties";
    public static final String GLP_CURENCY_LIST = "glp-currency-list.properties";
    public static final String PROD_VAT2_CURRENCY_FILTER = "prod-vat2-currency-filter";
    public static final String PROD_VAT2_NGLP_CURRENCY_FILTER = "prod-vat2-nglp-currency-filter";
    public static final String PROD_VAT2_ERROR = "prod-vat2-error";


}
