package com.postnord.nglp;

import com.postnord.nglp.internal.Constants;
import com.postnord.nglp.internal.TestHelper;
import com.postnord.nglp.internal.TransactionSerde;
import com.postnord.nglp.model.EventType;
import com.postnord.nglp.processor.Processor;
import com.postnord.nglp.util.StringToEventMapper;
import com.postnord.potapi.model.Transaction;
import org.apache.commons.io.FileUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.math.BigDecimal;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public class ProcessorTest {

    public static final String APPLICATION_ID = "potapi-input-test";
    private TopologyTestDriver topologyTestDriver;

    @Before
    public void setUp(){
        Properties props = new Properties();
        Processor processor = new Processor();
        props.put(StreamsConfig.CLIENT_ID_CONFIG, "potapi-input-test-client");
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "potapi");
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, APPLICATION_ID);
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.REPLICATION_FACTOR_CONFIG, 1);
        props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);
        Topology topology = processor.topology();
        topologyTestDriver = new TopologyTestDriver(topology,props);
    }

    @Test
    public void shouldWriteToPotapiTransactionRead() throws Exception{
        String inputXml = TestHelper.getXml("glp_se_customs_event.xml");
        assertThat(inputXml).isNotNull();
        writeInputToTopology(inputXml);
        EventType event = getEventType(inputXml);

        ProducerRecord<String, Transaction> transactions = readTransaction();
        Transaction tx = transactions.value();
        assertThat(tx).isNotNull();
        assertThat(tx.getAction().getAction()).isEqualTo("write");
        assertThat(tx.getRetries().getRetries()).isEqualTo(0);
        assertThat(tx.getActor().getActor()).isEqualTo("GLP");
        assertThat(tx.getEventTime()).isNotNull();
        assertThat(tx.getTransactionId().getTransactionId()).isNotNull();

        //Payload test
        assertThat(tx.getPayload()).isNotNull();
        assertThat(tx.getPayload().getItems()).isNotNull();
        assertThat(tx.getPayload().getOrders()).isNotNull();
        assertThat(tx.getPayload().getItems().get(0).getItemId()).isNotNull();
        assertThat(tx.getPayload().getItems().get(0).getPnItemId().getPnItemId()).isEqualTo(event.getId());
        assertThat(tx.getPayload().getItems().get(0).getProductCode().getProductCode()).isEqualTo(event.getService().getCode());
        assertThat(tx.getPayload().getItems().get(0).getServiceCodes().get(0).getServiceCode()).isEqualTo(event.getAdditionalService().getCode());
        assertThat(tx.getPayload().getItems().get(0).getProcessingTime().getProcessingTime()).isNotNull();
        assertThat(tx.getPayload().getItems().get(0).getWeight().getWeight()).isEqualTo(event.getCustomsLabel().getTotalStatedWeight().getValue().multiply(BigDecimal.valueOf(1000)).intValue());
        assertThat(tx.getPayload().getItems().get(0).getCustoms()).isNotNull();
        assertThat(tx.getPayload().getItems().get(0).getCustoms().getOriginals()).isNotNull();
        assertThat(tx.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getCommercialRecipient().getGift().booleanValue()).isEqualTo(event.getCustomsLabel().isIsCommercialRecipient().booleanValue());
        assertThat(tx.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getGoodsDescription().getGoodsCategory()).isEqualTo(event.getCustomsLabel().getItemDescription());
        assertThat(tx.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getGoodsValue().getGoodsValue().intValue()).isEqualTo(event.getCustomsLabel().getTotalStatedValue().getValue().intValue());
        assertThat(tx.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getGoodsValue().getCurrency().getCurrencyCode()).isEqualTo(event.getCustomsLabel().getTotalStatedValue().getCurrency());
        assertThat(tx.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getIsGift().getGift().booleanValue()).isEqualTo(event.getCustomsLabel().isIsGift().booleanValue());
        assertThat(tx.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getIsDocument().getDocument().booleanValue()).isEqualTo(event.getCustomsLabel().isIsDocument().booleanValue());
        assertThat(tx.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getHsTariffNumber().getHsTariffNumber()).isEqualTo(event.getLocalCode().getValue());
        assertThat(tx.getPayload().getItems().get(0).getCustoms().getOriginals().get(0).getIsSample().getSample().booleanValue()).isEqualTo(event.getCustomsLabel().isIsCommercialSample().booleanValue());

        assertThat(tx.getPayload().getOrders().get(0)).isNotNull();
        assertThat(tx.getPayload().getOrders().get(0).getConsignor()).isNotNull();
        assertThat(tx.getPayload().getOrders().get(0).getConsignee()).isNotNull();
        assertThat(tx.getPayload().getOrders().get(0).getConsignor().getOriginal().getCountry().getCountry()).isEqualTo(event.getConsignor().getAddress().getCountry());

        assertThat(tx.getPayload().getOrders().get(0).getConsignee().getOriginal().getName().getName()).isEqualTo(event.getConsignee().getName());
        assertThat(tx.getPayload().getOrders().get(0).getConsignee().getOriginal().getCity().getCity()).isEqualTo(event.getConsignee().getAddress().getCity());
        assertThat(tx.getPayload().getOrders().get(0).getConsignee().getOriginal().getCountry().getCountry()).isEqualTo(event.getConsignee().getAddress().getCountry());
        assertThat(tx.getPayload().getOrders().get(0).getConsignee().getOriginal().getPostalCode().getPostalCode()).isEqualTo(event.getConsignee().getAddress().getPostcode());
        assertThat(tx.getPayload().getOrders().get(0).getConsignee().getOriginal().getAddressRow().getAddressRow()).isEqualTo(event.getConsignee().getAddress().getStreet1()
                + event.getConsignee().getAddress().getStreet2());

    }

    @Test
    public void shouldWriteToErrorTopic() throws  Exception{

        String inputXml = TestHelper.getXml("currency_error.xml");
        assertThat(inputXml).isNotNull();
        writeInputToTopology(inputXml);

        ProducerRecord<String, String> errors = readErrors();
        String error = errors.value();
        assertThat(error).isNotNull();
    }

    private EventType getEventType(String inputXml){

        StringToEventMapper stringToEventMapper = new StringToEventMapper();
        return stringToEventMapper.apply(inputXml);
    }

    private ProducerRecord<String, String> readErrors() {
        ProducerRecord<String, String> result = topologyTestDriver.readOutput(
                Constants.PROD_VAT2_ERROR, Serdes.String().deserializer(),
                Serdes.String().deserializer());
        if (result == null) {
            return null;
        }
        return result;
    }

    private ProducerRecord<String, Transaction> readTransaction() {
        ProducerRecord<String, Transaction> result = topologyTestDriver.readOutput(
                Constants.POTAPI_TRANSACTION_READ, Serdes.String().deserializer(),
                new TransactionSerde().deserializer());
        if (result == null) {
            return null;
        }
        return result;
    }

    private void writeInputToTopology(String inputXml) throws  Exception{

        ConsumerRecordFactory<String, String> factory =  new ConsumerRecordFactory<>(
                Serdes.String().serializer(),Serdes.String().serializer());

        topologyTestDriver.pipeInput(TestHelper.getValues(Constants.CURENCY_LIST,Constants.PROD_VAT2_CURRENCY_FILTER));
        topologyTestDriver.pipeInput(TestHelper.getValues(Constants.GLP_CURENCY_LIST,Constants.PROD_VAT2_NGLP_CURRENCY_FILTER));
        topologyTestDriver.pipeInput(factory.create(Constants.PROD_GLP_SE_CUSTOMS_EVENT,"",inputXml));
    }

    @After
    public void cleanUp(){
        try {
            FileUtils.deleteDirectory(new File("/tmp/kafka-streams/" + APPLICATION_ID));
            FileUtils.forceMkdir(new File("/tmp/kafka-streams/" + APPLICATION_ID));
        } catch( Exception e) {
        }finally {
            topologyTestDriver.close();
        }
    }

}
